package main

import (
	"net/http"
	"math"
	"time"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	temps := prometheus.NewSummary(prometheus.SummaryOpts{
		Name:       "demo_temperature_summary",
		Help:       "The temperature of the frog pond.",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	})
	prometheus.MustRegister(temps)
	go func() {
		// Simulate some observations.
		for {
			for i := 0; i < 1000; i++ {
				temps.Observe(30 + math.Floor(120*math.Sin(float64(i)*0.1))/10)
			}

			time.Sleep(1 * time.Second)
		}
		
	}()

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}
