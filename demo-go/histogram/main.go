package main

import (
	"math/rand"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	durations := prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "pond_duration_req",
		Help:    "The duration of request process.", // Sorry, we can't measure how badly it smells.
		Buckets: prometheus.LinearBuckets(0, 0.2, 5),  // 5 buckets, each 0.2 centigrade wide.
	})
	prometheus.MustRegister(durations)
	go func() {
		for {
			rand.Seed(time.Now().UnixNano()) //设置随机种子，使每次结果不一样
			i := float64(rand.Intn(100)) / 100
			durations.Observe( i)
			
			time.Sleep(1 * time.Second)
		}
	}()
	
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}