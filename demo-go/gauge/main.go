package main

import (
	"net/http"
	"time"
	"math/rand"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	opsQueued := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "our_company",
		Subsystem: "blob_storage",
		Name:      "ops_queued",
		Help:      "Number of blob storage operations waiting to be processed.",
	})
	prometheus.MustRegister(opsQueued)
	go func() {
		for {
			num := rand.Intn(10)
			if num > 5 {
				opsQueued.Add(float64(num) - 5)
			} else {
				opsQueued.Sub(float64(num))
			}
			
			time.Sleep(2 * time.Second)
		}
	}()
	
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}