package main

import (
	"net/http"
	"math"
	"math/rand"
	"time"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	temps := prometheus.NewSummary(prometheus.SummaryOpts{
		Name:       "demo_temperature_summary",
		Help:       "The temperature of the frog pond.",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	})
	prometheus.MustRegister(temps)
	durations := prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "pond_duration_req",
		Help:    "The duration of request process.", // Sorry, we can't measure how badly it smells.
		Buckets: prometheus.LinearBuckets(0, 0.2, 5),  // 5 buckets, each 0.2 centigrade wide.
	})
	prometheus.MustRegister(durations)
	opsQueued := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "our_company",
		Subsystem: "blob_storage",
		Name:      "ops_queued",
		Help:      "Number of blob storage operations waiting to be processed.",
	})
	prometheus.MustRegister(opsQueued)
	opsProcessed := promauto.NewCounter(prometheus.CounterOpts{
		Name: "myapp_processed_ops_total",
		Help: "The total number of processed events",
	})
	go func() {
		// Simulate some observations.
		for {
			for i := 0; i < 1000; i++ {
				temps.Observe(30 + math.Floor(120*math.Sin(float64(i)*0.1))/10)
			}

			rand.Seed(time.Now().UnixNano()) //设置随机种子，使每次结果不一样
			i := float64(rand.Intn(100)) / 100
			durations.Observe( i)

			num := rand.Intn(100)
			opsQueued.Set(float64(num))

			opsProcessed.Inc()

			time.Sleep(1 * time.Second)
		}
		
	}()

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}
